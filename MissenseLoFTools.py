import re
import os

"""
MissenseLoFTools

A module of classes for estimating the expected number of missense disruption-of-function variants for the genomes of a transcriptome.

Has classes:
- to parse the empirical trinucleotide mutation rates
- to parse and retrieve the exhaustive prediction of DDG scores for all amino acid substitutions possible in all UniPro/AlphaFold2 proteins
- to calculate the expected variant counts, given the mutation rate and the size of the population
- and utility classes

An example application of this module to compile expected and observed missense DoF variants in the GnomAD dataset can be found in the script:

quickstart_missense_dof.py

"""

#MAESTRO_RESULTS_DIRECTORY = '/media/andrews/ExPhATstick/maestro_out/stabilityPredictions/' 

class UniversalGeneticCode:
    """ A class that encapsulates the universal genetic code, to allow easy translation of nucleotide codons to amino acids. """
    
    def __init__(self):
        self.code = {
            'TCA': 'S', # Serine
            'TCC': 'S', # Serine
            'TCG': 'S', # Serine
            'TCT': 'S', # Serine
            'TTC': 'F', # Phenylalanine
            'TTT': 'F', # Phenylalanine
            'TTA': 'L', # Leucine
            'TTG': 'L', # Leucine
            'TAC': 'Y', # Tyrosine
            'TAT': 'Y', # Tyrosine
            'TAA': '_', # Stop
            'TAG': '_', # Stop
            'TGC': 'C', # Cysteine
            'TGT': 'C', # Cysteine
            'TGA': '_', # Stop
            'TGG': 'W', # Tryptophan
            'CTA': 'L', # Leucine
            'CTC': 'L', # Leucine
            'CTG': 'L', # Leucine
            'CTT': 'L', # Leucine
            'CCA': 'P', # Proline
            'CAT': 'H', # Histidine
            'CAA': 'Q', # Glutamine
            'CAG': 'Q', # Glutamine
            'CGA': 'R', # Arginine
            'CGC': 'R', # Arginine
            'CGG': 'R', # Arginine
            'CGT': 'R', # Arginine
            'ATA': 'I', # Isoleucine
            'ATC': 'I', # Isoleucine
            'ATT': 'I', # Isoleucine
            'ATG': 'M', # Methionine
            'ACA': 'T', # Threonine
            'ACC': 'T', # Threonine
            'ACG': 'T', # Threonine
            'ACT': 'T', # Threonine
            'AAC': 'N', # Asparagine
            'AAT': 'N', # Asparagine
            'AAA': 'K', # Lysine
            'AAG': 'K', # Lysine
            'AGC': 'S', # Serine
            'AGT': 'S', # Serine
            'AGA': 'R', # Arginine
            'AGG': 'R', # Arginine
            'CCC': 'P', # Proline
            'CCG': 'P', # Proline
            'CCT': 'P', # Proline
            'CAC': 'H', # Histidine
            'GTA': 'V', # Valine
            'GTC': 'V', # Valine
            'GTG': 'V', # Valine
            'GTT': 'V', # Valine
            'GCA': 'A', # Alanine
            'GCC': 'A', # Alanine
            'GCG': 'A', # Alanine
            'GCT': 'A', # Alanine
            'GAC': 'D', # Aspartic Acid
            'GAT': 'D', # Aspartic Acid
            'GAA': 'E', # Glutamic Acid
            'GAG': 'E', # Glutamic Acid
            'GGA': 'G', # Glycine
            'GGC': 'G', # Glycine
            'GGG': 'G', # Glycine
            'GGT': 'G',  # Glycine
            }

    def get_aa_for_codon(self, codon):
        """
        Simple method to return the encoded amino acid for a codon parameter.

        Parameters
        ----------
        codon : str
            Nucleotide codon sequence, eg. 'ATA'.  Returns a single character of the encoded AA, eg. 'I'.
        """
        
        return self.code[codon]

    
    
class TrinucleotideMutationRates:
    """ 
    This class reads an external data file of empirical trinucleotide mutation rates, such as
    those compiled from to calculate constraint metrics from GnomAD r2.1.1.  The path to this 
    file is presently HARDCODED.  File is included in the supplementary data from the GnomAD paper:
    GnomAD_2020_Karczewski_etal_supplementary_dataset_10_mutation_rates_full_trinucl.tsv

    See GnomAD paper: https://doi.org/10.1038/s41586-020-2308-7
    Supp Data Download: https://static-content.springer.com/esm/art%3A10.1038%2Fs41586-020-2308-7/MediaObjects/41586_2020_2308_MOESM4_ESM.zip

    Attributes:
    -----------

    trinuc_freq_file - the path to the supplementary dataset 10 file from the Karczewski et al 202 GnomAD manuscript (https://static-content.springer.com/esm/art%3A10.1038%2Fs41586-020-2308-7/MediaObjects/41586_2020_2308_MOESM4_ESM.zip)

    Methods:
    --------

    get(ref_tn (str), var_tn (str)) : returns str
        Returns the empirical mutation rate between the reference trinucleotide (say, 'AAA') and a variant trinucleotide (say, 'ATA').

    store(ref_tn (str), var_tn (str), mutation_rate (str)) : 
        Can manually store mutation rates with this method, yet ideally all mutation rates are read from a file at instantiation time.

    dump() :
        Prints to STDOUT the table of ref and var trinucleotides and their mutation frequencies.
    """
    
    def __init__(self, trinuc_freq_file):
        """
        Parameters
        ----------

        trinuc_freq_file - the path to the supplementary dataset 10 file from the Karczewski et al 202 GnomAD manuscript

        """
        
        self.input_filename = trinuc_freq_file
        self.trinucleotide_mut_freqs = dict()
        
        try:
            trinucl_in = open(self.input_filename, 'rt')
        except:
            print("Unable to open file [" + self.input_filename + "]")

        # context ref     alt     methylation_level       mu_snp
	# AAA     A       C       0       1.8303e-09
	# AAA     A       G       0       3.0288e-09
	# AAA     A       T       0       1.0670e-09
	# AAC     A       C       0       1.3911e-09
	# AAC     A       G       0       5.2457e-09
            
        while True:
            line = trinucl_in.readline()

            if not line:
                break

            line = line.replace("\n", "")

            ref_tn, ref_nt, var_nt, meth_level, mutation_rate = line.split("\t")
            
            if ref_tn == 'context': # this is the header line, so...
                continue
            
            result = re.match('[ATGC][ATGC][ATGC]', ref_tn)
            
            if result is not None: # if a complete trinucleotide has been parsed
                var_tn = ref_tn[0] + var_nt + ref_tn[2]
                
                self.store(ref_tn, var_tn, mutation_rate)

            else:
                raise Exception("TrinucleotideMutationRates init: Funny looking trinucl [" + ref_tn + "]")

            
    def store(self, ref_tn, var_tn, mutation_rate):
        """
        Stores the frequency of a given ref_tn to var_tn mutation.  This probably is only going to be used my the class itself.

        Parameters
        ----------
        ref_tn : str
             Three character nucleotide sequence of the unmutated trinucleotide, like 'AAA'.

        var_tn : str
             Three character nucleotide sequence of the mutated trinucleotide, like 'ATA'.

        mutation_rate : str
             A string representation of the ref->var mutation rate, eg. '1.0670e-09'


        """

        if not ref_tn in self.trinucleotide_mut_freqs:
            self.trinucleotide_mut_freqs[ref_tn] = dict()

        self.trinucleotide_mut_freqs[ref_tn][var_tn] = mutation_rate


    def get(self, ref_tn, var_tn):
        """ 
        Gets the frequency of mutations from the reference trinucleotide to the variant trinucleotide.

x        Parameters:
        -----------
        ref_tn : str
             Three character nucleotide sequence of the unmutated trinucleotide, like 'AAA'.

        var_tn : str
             Three character nucleotide sequence of the mutated trinucleotide, like 'ATA'.

        Raises:
        -------
        Exception
            If the reference and var trinucleotides are the same.
            If (for some reason) the ref or var trinucleotide is not defined.

        """

        if ref_tn == var_tn:
            raise Exception("The reference trinucleotide is the same as the variant trinucleotide provided.  Check your code.")
        
        if ref_tn in self.trinucleotide_mut_freqs:

            if var_tn in self.trinucleotide_mut_freqs[ref_tn]:

                return self.trinucleotide_mut_freqs[ref_tn][var_tn]
            else:
                raise Exception("TrinucleotideMutationRates get: Unable to retrieve the var trinucleotide [ref:" + ref_tn + "][var:" + var_tn + "]")
                
        else:
            raise Exception("TrinucleotideMutationRates get: Unable to retrieve the ref trinucleotide [ref:" + ref_tn + "]")

        
    def dump(self):
        """ 
        Utility method to dump to screen the ref-> var mutation frequency data.
        """
        
        for ref_tn in self.trinucleotide_mut_freqs.keys():
            print(ref_tn)
            for var_tn in self.trinucleotide_mut_freqs[ref_tn].keys():
                print("\t" + var_tn + ": " + self.trinucleotide_mut_freqs[ref_tn][var_tn])


                
class Blacklist:
    """
    Class to manage a dict of blacklisted strings - in this case, the UniProt ids of proteins with funny structures, bad transcript or something else.

    Parameters:
    -----------
    none needed

    Methods:
    --------
    add : str
        add a new blacklisted identifier.  Probably best called by the class itself, use increment instead.

    increment : str
        increment the count of a blacklisted identifier.

    check : str
        check if an indentifier is in the blacklist 
    """
    
    def __init__(self):
        self.blackdict = dict()

    def add(self, identifier):
        """ Internally adds an identifier to the blacklist.  You probably want to use 'increment' instead. """
        if identifier in self.blackdict.keys():
            self.increment(identifier)
        else:
            self.blackdict[identifier] = 1

    def increment(self, identifier):
        if identifier not in self.blackdict.keys():
            self.add(identifier)
        else:
            self.blackdict[identifier] += 1
        
    def check(self, identifier):
        try:
            result = identifier in self.blackdict.keys()
            return True
        except KeyError:
            return False

                
class StabilityChange:
    """
    Parses the output files from the exhaustive computation of MAESTRO against all human protein structures.  There is an
    output file per protein, so there are 20k+ of these.  Every one of these will be read during runtime, so perhaps use 
    the fastest drive you have, such as a good SSD. 

    The method get_ddG will trigger the searching for and the parsing of the appropriately named MAESTRO output file.  The last called UniProt
    id results are held in memory, so working protein-by-protein is actually quite efficient.

    Parameters:
    -----------
    maestro_results_directory : 
         directory path of all MAESTRO output files.  Directory contains all output files of MAESTRO predictions for every human protein.

    fail_threshold : int
         This is how many mismatches of amino acid position with amino acid coordinate are allowed before the UniProt id is blacklisted.

    verbose : 
         If you want lots of debug printing to the screen, set this to True.  Set to False by default.

    Methods:
    --------
    get_ddG : 
         does what it says and returns the parsed ddG value for a Uniprot Id, an amino acid coordinate and an amino acid substitution.

    parse_uniprotid_maestro_output_to_mem : 
         an internal method that parses a MAESTRO output file for a given UniProt id.  Holds this in memory for multiple queries of the same protein.
    """
    
    def __init__(self, maestro_results_directory = None, fail_threshold = 25, verbose = False):
        self.maestro_results_directory = maestro_results_directory 
        self.uniprotid_in_mem = None
        self.maestro_results_in_mem = dict()
        self.ref_aa_lookup_in_mem = dict()
        self.transcript_blacklist = Blacklist()
        self.uniprotid_blacklist = Blacklist()
        self.retrieval_failed_threshold = fail_threshold # (25 is stringent) This threshold is the number of times a call for a ddG value from a specific uniprotid fails - this failure can be due to a missing file (UniProt ID not run or missing) or because there are lots of Ref_AA mismatches (maybe because of the wrong transcript is being used).  Either way, it is best to skip if this happens much. The number can be large, even if there is a single reference AA mismatch, as many calls at a site AA site take place.
        self.verbose = verbose

        if self.maestro_results_directory is None:
            raise Exception("Directory with exhaustively computed MAESTRO output has not been set.  Bad things will happen.")
        
    def get_ddG(self, uniprot_id, aa_pos, ref_aa, var_aa):

        # filter requests for wildtype vars - youd think the calling code would have filtered these already
        if ref_aa == var_aa:
            if self.verbose:
                print("Making a call to StabilityChange.get_ddG() with ref_aa [" + ref_aa + "] the same as var_aa [" + var_aa + "].")
            return 0.0
        
        # if this uniprot_id is not already in memory, parse now
        if uniprot_id != self.uniprotid_in_mem:
            self.parse_uniprotid_maestro_output_to_mem(uniprot_id)

        # check for cases where the supplied reference AA is not the reference AA in the UniProt protein
        # -- "Reasons for this:\n"
	# --  "  1) The protein may be very long - and the Maestro output was split into several output files.  There are only 200 of these in the human genome.\n"
	# --  "  2) The reference amino acid single letter code does not match that in the Maestro output.  This happens mostly when the wrong splice forms transcript<->protein are matched.  There are often several CCDS transcripts per gene.";

        # Both cases lead to this block returning a None.  If the aa position is found and the aa matches the reference aa, then it sails through this code without hinderance
        try:
            if aa_pos in self.ref_aa_lookup_in_mem.keys():
                if self.ref_aa_lookup_in_mem[aa_pos] != ref_aa:
                    if self.verbose:
                        print("Request for an amino acid position that was not in the Maestro output.") # wont also increment the transcript blacklist counter, this better done by the calling code
                    return None
            else:
                pass
        except:
            if self.verbose:
                print("Trying to retrieve an amino acid position that is not in the lookup [aa_pos:" + str(aa_pos)
                      + "][ref_aa:" + ref_aa + "].  This amino acid is probably past the end of the Maestro output... ignore, for now.")
            return None
                
        # All good.  Return ddG value from MAESTRO output
        mutation_key = ref_aa + str(aa_pos) + var_aa

        if mutation_key in self.maestro_results_in_mem.keys():
            if self.verbose:
                print("Fetching : " + uniprot_id + " " + mutation_key + " ddG:  " + str(self.maestro_results_in_mem[mutation_key]))

            return self.maestro_results_in_mem[mutation_key]

        else:
            return None
        
    def parse_uniprotid_maestro_output_to_mem(self, uniprot_id):

#        maestro_out_file = MAESTRO_RESULTS_DIRECTORY + "/AF-" + uniprot_id + "-F1.maestro.txt"
        maestro_out_file = self.maestro_results_directory + "/AF-" + uniprot_id + "-F1.maestro.txt" 

        if self.verbose:
            print("Reading results from file [" + maestro_out_file + "]")
            
        self.uniprotid_in_mem = uniprot_id

        if os.path.exists(maestro_out_file):            
            try:
                maestro_in = open(maestro_out_file, 'rt')                
            except:
                raise Exception("Unable to open file [" + maestro_out_file + "]")
                
            # Parse MAESTRO results file
            while True:
                line = maestro_in.readline()

                if not line:
                    break

                # #structure      seqlength       pH      mutation        score   delta_score     ddG     ddG_confidence
                # /g/...-Q9UPC5-F1-model_v1.pdb,A,0          381    7.0   wildtype              -9.91858         0.00000         0.00000         1.00000  
                # /g/...-Q9UPC5-F1-model_v1.pdb,A,0          381    7.0   M1.A{A}       -9.92640        -0.00782         1.33895         0.67652  
                # /g/...-Q9UPC5-F1-model_v1.pdb,A,0          381    7.0   M1.A{V}       -9.91342         0.00516         1.25413         0.69583  
                # /g/...-Q9UPC5-F1-model_v1.pdb,A,0          381    7.0   M1.A{Y}       -9.89219         0.02639         0.75527         0.72383  
                # #structure      seqlength       pH      mutation        score   delta_score     ddG     ddG_confidence
                # /g/...-Q9UPC5-F1-model_v1.pdb,A,0          381    7.0   wildtype              -9.91858         0.00000         0.00000         1.00000  
                # /g/...-Q9UPC5-F1-model_v1.pdb,A,0          381    7.0   R2.A{A}       -9.87893         0.03965         1.64262         0.68012  

                # skip all the header lines, which can be inside the file rather than just the top line
                is_header = re.match('#', line) # at beginning of line
            
                if is_header is not None: # this line begins with a '#'.  Skip.
                    continue

                # skip if this isnt really a substitution
                is_not_aa_change = re.search('wildtype', line) # anywhere in line, but always col 5

                if is_not_aa_change is not None: # line contains 'wildtype'
                    continue

                # parse line
                line = line.replace("\n", "")

                cols = line.split("\t")
                mutation = cols[3]
                ddG = cols[6]
                conf = cols[7]

                # parse mutation
                match = re.search(r'([A-Z])(\d+)\.[A-Z]\{([A-Z])\}', mutation)
                if match is not None: # mutation is formatted as expected
                    parsed_ref_aa, parsed_aa_pos, parsed_var_aa = match.groups()
                else:
                    raise Exception("Did not parse mutation in MAESTRO output [mutation:" + mutation + "]")

                # store mutation
                mutation_key = parsed_ref_aa + parsed_aa_pos + parsed_var_aa
                self.maestro_results_in_mem[mutation_key] = float(ddG)
                self.ref_aa_lookup_in_mem[parsed_aa_pos] = parsed_ref_aa
                                    
                if self.verbose:
                    print("MAESTRORESULT_IN_MEM: " + parsed_ref_aa + parsed_aa_pos + parsed_var_aa + "\t" + ddG)
                    print("REF_AA_LOOKUP: " +  parsed_aa_pos + "\t" + parsed_ref_aa)

        else:
            if self.verbose:
                print("Unable to find file [" + maestro_out_file + "].  Backlisting " + uniprot_id)
            self.uniprotid_blacklist.add(uniprot_id)

            return None



class ExpectedMutations:
    """
    In lieu of detailed docs, this code is called by the script:  get_expected_syn_nonsyn_misLoF_mutants_per_gene_b37p13_Gnom211.py

    Reading the script might make it clearer how this is all put together.  Especially how all the classes are pulled together to construct 
    a data munging worklow with the GnomAD data.

    This class implements a method to calculate the number of expected mutations with stability effects on the encoded protein.  It modifies 
    the method for calculating the expected number of mutations for a given gene described in the following papers:

    GnomAD paper --> https://doi.org/10.1038/s41586-020-2308-7
    Neale et al -> https://doi.org/10.1038/nature11011

    Parameters:
    -----------
    upper_ddG_threshold : 
    lower_ddG_threshold :
        the thresholds of ddG, beyond which an AA substitution can be classed as causing instability on a given protein

    maestro_out_dir :
        full path to directory containing all MAESTRO prediction output files, one per protein.

    """

    def __init__(self, upper_ddG_threshold = 0, lower_ddG_threshold = 0, maestro_out_dir = None, verbose = False):
        self.verbose = verbose
        if maestro_out_dir == None:
            raise Exception("maestro_out_dir parameter not set.  This is the full path to the directory containing to MAESTRO output files for all proteins.")

        self.maestro_out_dir = maestro_out_dir

        self.transcript_blacklist = Blacklist()
        self.genetic_code = UniversalGeneticCode()
        self.stability_change = StabilityChange(verbose=self.verbose, maestro_results_directory = self.maestro_out_dir)
        
        self.counts_possible_missense_lof_mutations = dict()  # <- nonsyn mutations outside thresholds
        self.counts_possible_nonsyn_mutations = dict() # these are horrible vars that are modified only in the method process_transcript().  Um.
        self.counts_possible_syn_mutations = dict()    # AND these possible syn counts are totally necessary, as the expected and observed syn mutant counts are calibrated to be equal, and thereby the expected nonsyn mutants are also calibrated.  ie.  Total syn counts is used to set the expected syn counts.  The same multiplier is then applied to nonsyn.
        self.mutations_appraised_by_gene_and_transcript = dict() # AND another ... and this just fills a necessary gap of all valid gene -> transcript sets (not all transcripts will map to AF2 models either)

        self.UPPER_DDG_THRESHOLD = upper_ddG_threshold
        self.LOWER_DDG_THRESHOLD = lower_ddG_threshold
        

    def get_possible_trinucl_by_gene(self, gene_sym = None, trinucl_counts = None):
        """
        Works processively through a transcript (the canonical transcript) for a gene, counting the numbers of potential mutants
        that cause a specific outcome (eg. synonymous, missense, missense-LoF change)

        The trinucleotide counts passed to the function (via trinucl_counts) define the type of mutants being counted.  Such as:

        self.get_possible_trinucl_by_gene(gene_sym = gene_sym, trinucl_counts = self.counts_possible_missense_lof_mutations)

        The counts of possible missense/synonymous/LoF mutations are all compiled by self.assess_and_count_mutation, which is
        called by self.process_transcript

        """

        if gene_sym is None:
            raise Exception("Call to get_possible_trinucl_by_gene needs gene_sym parameter to be not None.")

        if trinucl_counts is None:
            raise Exception("Call to get_possible_trinucl_by_gene needs trinucl_counts parameter to be not None.")

        if gene_sym.upper() in self.counts_possible_nonsyn_mutations.keys():

            these_transcripts = list()
            for transcript_key in trinucl_counts[gene_sym.upper()].keys():
                these_transcripts.append(transcript_key)

            this_transcript = None
                
            if len(these_transcripts) == 1:
                this_transcript = these_transcripts[0]
            else:
                this_transcript = these_transcripts[0]
                if self.verbose:
                    print("Multiple canonical transcripts held for gene [" + gene_sym + "].  Going to just choose the first one [" + this_transcript + "].")

            if self.verbose:
                print("\t".join(("Gene_Symbol","Canonical_Transcript", "Ref_Trinucl", "Mut_Trinucl", "Count")))
            
            for this_ref_trinucl in trinucl_counts[gene_sym.upper()][this_transcript].keys():
                for this_mut_trinucl in trinucl_counts[gene_sym.upper()][this_transcript][this_ref_trinucl].keys():
                    this_counts = trinucl_counts[gene_sym.upper()][this_transcript][this_ref_trinucl][this_mut_trinucl]
                    if self.verbose:
                        print("\t".join((gene_sym, this_transcript, str(this_ref_trinucl), str(this_mut_trinucl), str(this_counts))))

            return trinucl_counts[gene_sym.upper()][this_transcript]

        else:
            if self.verbose:
                print("No trinucleotide counts data is held for gene [" + gene_sym + "]")                
            return None


    def get_possible_missense_lof_trinucl_mutations_by_gene(self, gene_sym = None):

        return self.get_possible_trinucl_by_gene(gene_sym = gene_sym, trinucl_counts = self.counts_possible_missense_lof_mutations)
        
    
    def get_possible_nonsyn_trinucl_mutations_by_gene(self, gene_sym = None):

        return self.get_possible_trinucl_by_gene(gene_sym = gene_sym, trinucl_counts = self.counts_possible_nonsyn_mutations)
        
    
    def get_possible_syn_trinucl_mutations_by_gene(self, gene_sym = None):

        return self.get_possible_trinucl_by_gene(gene_sym = gene_sym, trinucl_counts = self.counts_possible_syn_mutations)

    
    def initialise_possible_counts_for_gene_and_trinucl_change(self, counts_dict, gene_name, transcript_id, ref_trinucl, mut_trinucl):

        # check args
        if counts_dict is None or type(counts_dict) is not dict:
            raise Exception("Expecting arg counts_dict to be a dict.")

        if gene_name is None or type(gene_name) is not str:
            raise Exception("Expecting gene_name to be a str.")

        if transcript_id is None or type(transcript_id) is not str:
            raise Exception("Expecting transcript_id to be a str")

        if ref_trinucl is None or type(ref_trinucl) is not str:
            raise Exception("Expecting ref_trinucl to be a str")

        if mut_trinucl is None or type(mut_trinucl) is not str:
            raise Exception("Expecting mut_trinucl to be a str")
        
        # build a multi-level data dict for storing trinucleotide mutation information.  Check not clobbering existing data (which might happen).  There is probably a better way.
        if gene_name.upper() not in counts_dict.keys():
            counts_dict[gene_name.upper()] = dict()

        if transcript_id not in counts_dict[gene_name.upper()].keys():
            counts_dict[gene_name.upper()][transcript_id] = dict()

        if ref_trinucl not in counts_dict[gene_name.upper()][transcript_id].keys():
            counts_dict[gene_name.upper()][transcript_id][ref_trinucl] = dict()

        if mut_trinucl not in counts_dict[gene_name.upper()][transcript_id][ref_trinucl].keys():
            counts_dict[gene_name.upper()][transcript_id][ref_trinucl][mut_trinucl] = 0

        return True
        
        
    def assess_and_count_mutation(self, gene_name, transcript_id, uniprot_id, amino_acid_position, codon, mut_codon, ref_trinucl, mut_trinucl):
        
        ref_aa = self.genetic_code.get_aa_for_codon(codon)
        mut_aa = self.genetic_code.get_aa_for_codon(mut_codon)
                
        if mut_aa == '_': # mutant encodes a Stop codon
            if self.verbose:
                print("Trying to assess_and_count_mutation on a Stop codon.  Returning a None.")

            return None
                    
        # Initialise the multi-dimensional dicts for this gene:
        self.initialise_possible_counts_for_gene_and_trinucl_change(self.counts_possible_nonsyn_mutations, gene_name, transcript_id, ref_trinucl, mut_trinucl) # nonsyn
        self.initialise_possible_counts_for_gene_and_trinucl_change(self.counts_possible_syn_mutations, gene_name, transcript_id, ref_trinucl, mut_trinucl) # syn
        self.initialise_possible_counts_for_gene_and_trinucl_change(self.counts_possible_missense_lof_mutations, gene_name, transcript_id, ref_trinucl, mut_trinucl) # missense lof

        # Is it a missense (nonsyn) mutation?
        if ref_aa != mut_aa:

            # Include this in the total missense count also
            self.counts_possible_nonsyn_mutations[gene_name.upper()][transcript_id][ref_trinucl][mut_trinucl] += 1

            # Check the stability change caused by this amino acid substitution
            this_ddg = self.stability_change.get_ddG(uniprot_id, amino_acid_position, ref_aa, mut_aa)

            if this_ddg is not None: # a ddG predicted value can be retrieved from the MAESTRO results
                if gene_name.upper() not in self.mutations_appraised_by_gene_and_transcript.keys():
                    self.mutations_appraised_by_gene_and_transcript[gene_name.upper()] = dict()

                    self.mutations_appraised_by_gene_and_transcript[gene_name.upper()][transcript_id] = 1

            else: # wasnt a predicted ddG for this missense mutation
                gene_transcript_key = gene_name + ":" + transcript_id
                self.transcript_blacklist.increment(gene_transcript_key) # count the number of times this transcript doesnt match the UniProt/MAESTRO results

                return None # done with the mutation (there isnt anything we can do with missing data)

            # Count possible mutant types in their trinucl context            
            if this_ddg >= self.UPPER_DDG_THRESHOLD or this_ddg <= self.LOWER_DDG_THRESHOLD:
                if self.verbose:
                    print("Counted above threshold nonsyn mutation at first codon position [gene:" + gene_name + "][transcript:" + transcript_id + "][ddG:" + str(this_ddg) + "]")

                # Count the instability-causing, possible missense mutant
                self.counts_possible_missense_lof_mutations[gene_name.upper()][transcript_id][ref_trinucl][mut_trinucl] += 1

        else: # from above ref_aa == mut_aa -- a synonymous mutation
            if self.verbose:
                print("Found syn mut: " + gene_name + "\t" + ref_trinucl + "\t" + mut_trinucl)

            # Count the possible syn mutant
            self.counts_possible_syn_mutations[gene_name.upper()][transcript_id][ref_trinucl][mut_trinucl] += 1


    def process_transcript(self, gene_name, uniprot_id, transcript_id, seq):

        # Trim gene_name and seq string (remove newlines, spaces, etc)

        gene_name.replace("\s+", "")
        seq.replace("\s+", "")

        # Format sequence and set up data store

        nucleotides = seq

        # Iterate through the sequence of the transcript sequence counting mutation effect codon-by-codon
    
        amino_acid_position = 1 # the ATG, but this is skipped straight away

        vars = ('A','T','G','C')

        tn_centre = 4

        while tn_centre < (len(nucleotides) - 4): # start at second codon (skip Start ATG) and finish at second last codon (omit Stop TAA) eg. ATG CGG == 012 345.  Work by each nucleotide, not by codons (though, obviously, compare codons for nonsyn/syn)
            
            amino_acid_position += 1

            codon      = nucleotides[tn_centre-1] + nucleotides[tn_centre] + nucleotides[tn_centre+1]
            ref_first  = nucleotides[tn_centre-1]
            ref_second = nucleotides[tn_centre]
            ref_third  = nucleotides[tn_centre+1]

	    # Work through first codon position and potential mutants

            for mut_nt in vars:

                if mut_nt == ref_first: # skip the case where the mutant is the same as the ref (no change)
                    continue
	    
                mut_codon = mut_nt + ref_second + ref_third

                ref_trinucl = nucleotides[tn_centre-2] + ref_first + ref_second
                mut_trinucl = nucleotides[tn_centre-2] + mut_nt + ref_second

                self.assess_and_count_mutation(gene_name, transcript_id, uniprot_id, amino_acid_position, codon, mut_codon, ref_trinucl, mut_trinucl)
                
	    # Second codon position

            for mut_nt in vars:

                if mut_nt == ref_second:
                    continue

                mut_codon = ref_first + mut_nt + ref_third

                ref_trinucl = ref_first + ref_second + ref_third
                mut_trinucl = ref_first + mut_nt + ref_third

                self.assess_and_count_mutation(gene_name, transcript_id, uniprot_id, amino_acid_position, codon, mut_codon, ref_trinucl, mut_trinucl)

	    # Third codon position

            for mut_nt in vars:

                if mut_nt == ref_third:
                    continue

                mut_codon = ref_first + ref_second + mut_nt

                ref_trinucl = ref_second + ref_third + nucleotides[tn_centre+2]
                mut_trinucl = ref_second + mut_nt + nucleotides[tn_centre+2]

                self.assess_and_count_mutation(gene_name, transcript_id, uniprot_id, amino_acid_position, codon, mut_codon, ref_trinucl, mut_trinucl)

                
            # Very importantly - at the end of this for loop block, increment the centre nucleotide counter into the centre of the next codon
            tn_centre += 3


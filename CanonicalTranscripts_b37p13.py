import re
import os

"""
A module to make it easier to work with the canonical transcripts for genes from a given transcriptome.  BUT the input fasta file of the transcripts needs to be the canonical transcripts first.  There is no internal filtering or logic.  You must give it the canonical transcripts, that you have probably dumped from BioMart.

Works with b37_p13 by default.  But there is no limitation to the assembly used.
"""

CANONICAL_TRANSCRIPTS_FILE = 'data/Human_b37_p13_canonical_transcripts_220308.fa'

class Transcript:
    """
    A simple class to hold the identifier constellation for a transcript (gene_name, ensg, enst, ensp, uniprot_id) AND the 
    nucleotide sequence too!
    """
    
    def __init__(self, gene_name = None, seq = None, ensg = None, enst = None, uniprot_id = None):
        self.gene_name = gene_name
        self.seq = seq
        self.ensg = ensg
        self.enst = enst
        self.uniprot_id = uniprot_id
        

class CanonicalTranscriptome:
    """
    A class that encapsulates all the transcripts read from a fasta file containing the canonical transcripts.

    All transcript that are read from the input fasta are held as Transcript objects.
    """
    
    def __init__(self, input_fasta = None, verbose = False):
        self.transcripts_by_gene_name = dict()
        self.transcripts_by_enst = dict()
        self.uniprot_ids_by_transcript = dict()
        self.gene_names = dict()
        self.verbose = verbose
        
        if input_fasta is not None:
            self.input_fasta = input_fasta
        else:
            self.input_fasta = CANONICAL_TRANSCRIPTS_FILE


    def read_transcripts_from_fasta_file(self, input_fasta = None):
        if input_fasta is not None:
            self.input_fasta = input_fasta
        else:
            if self.input_fasta is None:
                raise Exception("Need to pass name of fasta file either to the constructor or as input_fast")

        if os.path.exists(self.input_fasta):            
            try:
                fasta_file = open(self.input_fasta, 'rt')                
            except:
                raise Exception("Unable to open file [" + self.input_fasta + "]")

        header = re.compile('^>')
        transcript_sequence = ""
        ensg = None
        enst = None
        uniprot_id = None
        gene_name = None
        
        while True: # each line in fasta file
            line = fasta_file.readline()
    
            if not line:
                break

            line = line.replace("\n", "") # remove newlines
            
	    # >ENSG00000003056|ENST00000000412|P20645|M6PR
	    # ATGTTCCCTTTCTACAGCTGCTGGAGGACTGGACTGCTACTACTACTCCTGGCTGTGGCA
	    # GTGAGAGAATCCTGGCAGACAGAAGAAAAAACTTGCGACTTGGTAGGAGAAAAGGGTAAA
	    #
	    # ensg|enst|uniprot_id|gene_name
            
            if header.match(line) is not None:
                # When we come to a new gene in the fasta file...
                # First, store the previous transcript, if there is one
                if gene_name is not None and enst is not None:
                    new_transcript = Transcript(gene_name  = gene_name,
                                                seq        = transcript_sequence,
                                                enst       = enst,
                                                ensg       = ensg,
                                                uniprot_id = uniprot_id)
                    
                    self.transcripts_by_gene_name[gene_name] = new_transcript
                    self.transcripts_by_enst[enst] = new_transcript
                    self.uniprot_ids_by_transcript[enst] = uniprot_id

                    # reset all the ids - I wonder if this does what I think?
                    gene_name = None
                    enst = None
                    ensg = None
                    uniprot_id = None
                    transcript_sequence = ""

                # Then, parse the header of the next transcript
                line = line.replace(">", "") # remove > from header
                ensg, enst, uniprot_id, gene_name = line.split("|")    

            else:
                transcript_sequence += line

            # remember to make transcript from the last sequence
            if gene_name is not None and enst is not None:
                new_transcript = Transcript(gene_name  = gene_name,
                                            seq        = transcript_sequence,
                                            enst       = enst,
                                            ensg       = ensg,
                                            uniprot_id = uniprot_id)
                    
                self.transcripts_by_gene_name[gene_name] = new_transcript
                self.transcripts_by_enst[enst] = new_transcript
                self.uniprot_ids_by_transcript[enst] = uniprot_id


























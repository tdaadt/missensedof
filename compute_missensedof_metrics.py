from MissenseLoFTools import TrinucleotideMutationRates
from MissenseLoFTools import ExpectedMutations
from ObservedMutationsGnomADr211 import ObservedMutations
from CanonicalTranscripts_b37p13 import CanonicalTranscriptome        
import getopt, sys

import pandas as pd
import numpy as np
from scipy.stats import shapiro, ttest_ind, pearsonr, poisson
from sklearn.linear_model import LinearRegression

"""
An example implementation of the classes within MissenseLoFTools

The workflow parses necessary external data: 
- trinucleotide mutation rates
- a canonical transcript set for human b37 genes
- observed variants from GnomAD r2.1.1

Then moves to calculating the multiplier between the possible synonymous variants in all genes and the numbers observed in GnomAD data

With this, then counts the numbers of missense and missense LoF variants for each transcript.  From this calculates the expected numbers
of each mutation class for each transcript.  Does not calculate nonsense/splicing LoF expected numbers.

Prints this data to STDOUT

Usage: python3 get_expected_syn_nonsyn_misLoF_mutants_per_gene_b37p13_Gnom211.py --upper_ddG_thresh 1.0 --lower_ddG_thresh -0.5 > minus0.5toDDGto1.0.tsv

###
python compute_missensedof_metrics.py --upper_ddG_thresh 1.0 --lower_ddG_thresh -0.5 --repo_data_dir data/ --maestro_out_dir /media/andrews/ExPhATstick/maestro_out/stabilityPredictions/ --gnomad_vcf /home/andrews/missensedof_gitlabrepo/detritus_not_in_repo/gnomad.exomes.r2.1.1.sites.missense_synonymous.head100000.vcf --downsample_to_1_in_every 1
###

"""

# REPO DATA FILES

TRINUC_FREQ_FILENAME = 'GnomAD_2020_Karczewski_etal_supplementary_dataset_10_mutation_rates_full_trinucl.tsv'
ID_MAPPINGS_FILENAME = 'Hsa_ensg_enst_uniprot_genesymb_b38p13_canonical.tsv'
CANONICAL_TRANSC_FILENAME = 'Human_b37_p13_canonical_transcripts_220308.fa'
LOF_METRICS_FILE = 'lof_metrics_r211_uniprot_canonical_transcript_mappings.tsv'


# Global ddG threshold DEFAULTS

UPPER_DDG_THRESH = 0.5
LOWER_DDG_THRESH = -0.5
DOWNSAMPLE_TO_1_IN_EVERY = 1 # default is no downsampling
DATA_DIR = './data/'
GNOMAD_VCF_FILENAME = None
MAESTRO_OUT_DIR = None

# (0) Get named command-line arguments, if any

commandArgs = sys.argv[1:] # keep args, minus the first one, which is the script name
print(sys.argv)
options = 'ulrmgd:'
long_options = ['upper_ddG_thresh=', 'lower_ddG_thresh=', 'repo_data_dir=', 'maestro_out_dir=', 'gnomad_vcf=', 'downsample_to_1_in_every=']

try:
    # getopt to parse arguments
    args, vals = getopt.getopt(commandArgs, options, long_options)

    # work through each argument and assign the passed value
    for current_opt, current_val in args:
        if current_opt in ["-u", "--upper_ddG_thresh"]:
            UPPER_DDG_THRESH = float(current_val)
        elif current_opt in ["-l", "--lower_ddG_thresh"]:
            LOWER_DDG_THRESH = float(current_val)
        elif current_opt in ["-d", "--downsample_to_1_in_every"]:
            DOWNSAMPLE_TO_1_IN_EVERY = int(current_val)
        elif current_opt in ["-r", "--repo_data_dir"]:
            DATA_DIR = current_val
        elif current_opt in ["-m", "--maestro_out_dir"]:
            MAESTRO_OUT_DIR = current_val
        elif current_opt in ["-g", "--gnomad_vcf"]:
            GNOMAD_VCF_FILENAME = current_val



except getopt.error as err:
    # output error, and return with an error code
    print (str(err))

print("upper_ddG_threshold: "+str(UPPER_DDG_THRESH))
print("lower_ddG_threshold: "+str(LOWER_DDG_THRESH))
print("downsample_to_1_in_every: "+str(DOWNSAMPLE_TO_1_IN_EVERY))
print("repo data directory: "+DATA_DIR)
print("MAESTRO out dir: "+MAESTRO_OUT_DIR)
print("GnomAD VCF file: "+GNOMAD_VCF_FILENAME)

# (1) Parse empirical trinucleotide frequencies from Karczewski et al

tmr = TrinucleotideMutationRates(trinuc_freq_file = DATA_DIR+"/"+TRINUC_FREQ_FILENAME)

# (2) Count the total synonymous variants for all genes (in the canonical transcript of each gene)

gnomad_mutations = ObservedMutations(
        observed_variation_file = GNOMAD_VCF_FILENAME,
        id_mapping_file = DATA_DIR + "/" +ID_MAPPINGS_FILENAME,
        verbose = False,
        maestro_out_dir = MAESTRO_OUT_DIR
        )

gnomad_mutations.parse_observed_input(
            upper_ddG_threshold=UPPER_DDG_THRESH,
            lower_ddG_threshold=LOWER_DDG_THRESH,
            downsample_to_1_in_every=DOWNSAMPLE_TO_1_IN_EVERY
            ) ### this is were the heavy lifting with the GnomAD input VCF takes place - can run for ages!

total_synonymous_variants = 0
total_missense_variants = 0
total_missense_lof_variants = 0

obs_syn_by_gene = dict()
obs_nonsyn_by_gene = dict()
obs_misLoF_by_gene = dict()

canonical_transcript_by_gene = dict()

for enst in gnomad_mutations.transcript_gene.keys(): # loop through the canonical transcript for each gene and count the observed GnomAD r2.1.1 variants (that are from the GnomAD VCF file)

    # skip genes without variants - a get out of jail when using testing with a sub-sampled test input file
    if gnomad_mutations.get_observed_synonymous_mutation_count_for_transcript(enst) is None:
        continue

    this_gene_name = gnomad_mutations.transcript_gene[enst]
    obs_syn_by_gene[this_gene_name] = gnomad_mutations.get_observed_synonymous_mutation_count_for_transcript(enst)
    obs_nonsyn_by_gene[this_gene_name] = gnomad_mutations.get_observed_missense_mutation_count_for_transcript(enst)
    obs_misLoF_by_gene[this_gene_name] = gnomad_mutations.get_observed_missense_LoF_count_for_transcript(enst)

    total_synonymous_variants += obs_syn_by_gene[this_gene_name]
    total_missense_variants += obs_nonsyn_by_gene[this_gene_name]
    total_missense_lof_variants += obs_misLoF_by_gene[this_gene_name]

    canonical_transcript_by_gene[this_gene_name] = enst


print("Total synonymous variants    : " + str(total_synonymous_variants))
print("Total missense variants      : " + str(total_missense_variants))
print("Total missense LoF variants  : " + str(total_missense_lof_variants))


# (3) Calculate sum of all the expected trinucleotide mutation frequencies that have a synonymous outcome

em = ExpectedMutations(upper_ddG_threshold=UPPER_DDG_THRESH, lower_ddG_threshold=LOWER_DDG_THRESH, maestro_out_dir=MAESTRO_OUT_DIR)

#-- Iterate through ALL CANONICAL TRANSCRIPT sequences (the input is _only_ canonical transcripts), counting possible synonymous mutations

ct = CanonicalTranscriptome()
ct.read_transcripts_from_fasta_file(input_fasta = DATA_DIR + '/' + CANONICAL_TRANSC_FILENAME)

syn_relative_by_gene = dict()
nonsyn_relative_by_gene = dict()
misLoF_relative_by_gene = dict()
total_relative_syn = 0
total_possible_syn_muts_by_gene = dict()
total_possible_nonsyn_muts_by_gene = dict()
total_possible_misLoF_muts_by_gene = dict()

gene_fate = dict()
gene_fate['skipped'] = list()
gene_fate['processed'] = list()

for gene_name in ct.transcripts_by_gene_name.keys(): # loop through the canonical transcript for each gene and count the observed GnomAD r2.1.1 variants (that are from the GnomAD VCF file)

    transcript = ct.transcripts_by_gene_name[gene_name]
    
    # skip genes without GnomAD variants - a get out of jail when using testing with a sub-sampled GnomAD test input file
    if gnomad_mutations.get_observed_synonymous_mutation_count_for_transcript(transcript.enst) is None:
        gene_fate['skipped'].append(gene_name)
        continue
    else:
        gene_fate['processed'].append(gene_name)

    ###
    #continue # completely short-circuit the rest of the run to gather stats only
    ###

    # Check that the canonical transcript parsed here with the CanonicalTranscriptome class actually matches with the canonical transcript parsed from the GnomAD VCF file

    if transcript.gene_name in canonical_transcript_by_gene.keys():

        fasta_seqs_ct = transcript.enst
        gnomad_vcf_ct = canonical_transcript_by_gene[transcript.gene_name]

    else:
        continue # for now, skip genes/transcripts that arent included in the GnomAD snippet file
            
    
    # Important - use the ExpectedMutation class (from MissenseLoFTools) to process this transcript to derive the trinucleotide site counts
    em.process_transcript(transcript.gene_name, transcript.uniprot_id, transcript.enst, transcript.seq)
    
    #-- COUNT the possible variants of each type present in this transcript

    #----- Count possible SYNONYMOUS variant sites per transcript

    gene_syn_trinucl_dict = em.get_possible_syn_trinucl_mutations_by_gene(gene_sym = transcript.gene_name)
    
    if gene_syn_trinucl_dict == None:
        continue

    for ref_trinucl in gene_syn_trinucl_dict.keys():

        if ref_trinucl not in gene_syn_trinucl_dict.keys(): # this ref trinucl doesnt exist in this gene, strange but possible
            continue
            
        for var_trinucl in gene_syn_trinucl_dict[ref_trinucl].keys():

            if var_trinucl not in gene_syn_trinucl_dict[ref_trinucl].keys(): # the ref trinucl is observed, but there are no mut trinucls attached.  Something went wrong...
                raise Exception("In gene [" + gene_name + "], there are no mutants attached to the ref trinucl [" + ref_trinucl + "]")

            this_possible_syn_trinucl_count = gene_syn_trinucl_dict[ref_trinucl][var_trinucl]

            if gene_name in total_possible_syn_muts_by_gene.keys():
                total_possible_syn_muts_by_gene[gene_name] += this_possible_syn_trinucl_count
            else:
                total_possible_syn_muts_by_gene[gene_name] = this_possible_syn_trinucl_count
                
            this_tn_mut_rate = tmr.get(ref_trinucl, var_trinucl)
            this_relative_syn_per_gene = this_possible_syn_trinucl_count * float(this_tn_mut_rate)

            if gene_name in syn_relative_by_gene.keys():
                syn_relative_by_gene[gene_name] += this_relative_syn_per_gene
            else:
                syn_relative_by_gene[gene_name] = this_relative_syn_per_gene

            total_relative_syn += this_relative_syn_per_gene

    #----- Count possible NON-SYNONYMOUS variant sites per transcript
    #-------> calculate the _relative_ non-synonymous proportions of sites per gene

    gene_nonsyn_trinucl_dict = em.get_possible_nonsyn_trinucl_mutations_by_gene(gene_sym = transcript.gene_name)

    for ref_trinucl in gene_nonsyn_trinucl_dict.keys():

        if ref_trinucl not in gene_nonsyn_trinucl_dict.keys(): # this ref trinucl doesnt exist in this gene, strange but possible
            continue
            
        for var_trinucl in gene_nonsyn_trinucl_dict[ref_trinucl].keys():

            if var_trinucl not in gene_nonsyn_trinucl_dict[ref_trinucl].keys(): # the ref trinucl is observed, but there are no mut trinucls attached.  Something went wrong...
                raise Exception("In gene [" + gene_name + "], there are no mutants attached to the ref trinucl [" + ref_trinucl + "]")

            this_possible_nonsyn_trinucl_count = gene_nonsyn_trinucl_dict[ref_trinucl][var_trinucl]

            if gene_name in total_possible_nonsyn_muts_by_gene.keys():
                total_possible_nonsyn_muts_by_gene[gene_name] += this_possible_nonsyn_trinucl_count
            else:
                total_possible_nonsyn_muts_by_gene[gene_name] = this_possible_nonsyn_trinucl_count
                
            this_tn_mut_rate = tmr.get(ref_trinucl, var_trinucl)
            this_relative_nonsyn_per_gene = this_possible_nonsyn_trinucl_count * float(this_tn_mut_rate)

            if gene_name in nonsyn_relative_by_gene.keys():
                nonsyn_relative_by_gene[gene_name] += this_relative_nonsyn_per_gene
            else:
                nonsyn_relative_by_gene[gene_name] = this_relative_nonsyn_per_gene


    #----- Count possible MISSENSE LOF variant sites per transcript
    #-------> calculate the _relative_ missense LoF proportions of sites per gene

    gene_misLoF_trinucl_dict = em.get_possible_missense_lof_trinucl_mutations_by_gene(gene_sym = transcript.gene_name)

    for ref_trinucl in gene_misLoF_trinucl_dict.keys():

        if ref_trinucl not in gene_misLoF_trinucl_dict.keys(): # this ref trinucl doesnt exist in this gene, strange but possible
            continue
            
        for var_trinucl in gene_misLoF_trinucl_dict[ref_trinucl].keys():

            if var_trinucl not in gene_misLoF_trinucl_dict[ref_trinucl].keys(): # the ref trinucl is observed, but there are no mut trinucls attached.  Something went wrong...
                raise Exception("In gene [" + gene_name + "], there are no mutants attached to the ref trinucl [" + ref_trinucl + "]")

            this_possible_misLoF_trinucl_count = gene_misLoF_trinucl_dict[ref_trinucl][var_trinucl]

            if gene_name in total_possible_misLoF_muts_by_gene.keys():
                total_possible_misLoF_muts_by_gene[gene_name] += this_possible_misLoF_trinucl_count
            else:
                total_possible_misLoF_muts_by_gene[gene_name] = this_possible_misLoF_trinucl_count
                
            this_tn_mut_rate = tmr.get(ref_trinucl, var_trinucl)
            this_relative_misLoF_per_gene = this_possible_misLoF_trinucl_count * float(this_tn_mut_rate)

            if gene_name in misLoF_relative_by_gene.keys():
                misLoF_relative_by_gene[gene_name] += this_relative_misLoF_per_gene
            else:
                misLoF_relative_by_gene[gene_name] = this_relative_misLoF_per_gene

                
print("Gene fate information (genes are skipped if they have no observed missense variants in gnomad):")
print("Skipped: " + str(len(gene_fate['skipped'])))
print("Processed: " + str(len(gene_fate['processed'])))


# (4) Observed synonymous count fit 

obs_syn_list = []
syn_rel_list = []

for gene in obs_syn_by_gene.keys():
    if gene in syn_relative_by_gene.keys():
        obs_syn_list.append(obs_syn_by_gene[gene])
        syn_rel_list.append(syn_relative_by_gene[gene])


obs_syn_array = np.array(obs_syn_list)
syn_rel_array = np.array(syn_rel_list)


obs_syn_fit = LinearRegression().fit(syn_rel_array.reshape(-1, 1), obs_syn_array.reshape(-1, 1))




# (5) Apply fit to each 'per gene relative syn'

expected_syn_by_gene = dict()
expected_nonsyn_by_gene = dict()
expected_misLoF_by_gene = dict()

all_genes = dict()

# -- Genes set1
genes_set1 = list(obs_syn_by_gene.keys())
for gene in genes_set1:
    all_genes[gene] = 1

# -- Genes set2
genes_set2 = list(total_possible_syn_muts_by_gene.keys())
for gene in genes_set2:
    if gene in all_genes.keys():
        all_genes[gene] = 2
    else:
        all_genes[gene] = 1

# -- Iterate through all genes observed so-far and print some data

for gene_name in all_genes.keys():

    if all_genes[gene_name] != 2:  # either there were no syn mutants in this gene, or there was no canonical transcript for this gene
        continue

    if not gene_name in canonical_transcript_by_gene.keys():
        continue
    
    expected_syn_by_gene[gene_name] = obs_syn_fit.predict(np.array([syn_relative_by_gene[gene_name]]).reshape(-1, 1))
    expected_nonsyn_by_gene[gene_name] = obs_syn_fit.predict(np.array([nonsyn_relative_by_gene[gene_name]]).reshape(-1, 1))
    expected_misLoF_by_gene[gene_name] = obs_syn_fit.predict(np.array([misLoF_relative_by_gene[gene_name]]).reshape(-1, 1))
    
    
# Include GnomAD annotations...
gnomad_data = pd.read_csv(DATA_DIR + '/' + LOF_METRICS_FILE, 
                delimiter="\t", 
                usecols=['Gene', 
                        'LOUEF_bin', 
                        'GnomAD_obs_lof',
                        'GnomAD_exp_lof', 
                        ]
                )

# Turn gnomad_data into a Numpy table
gnomad_data_np = pd.DataFrame.to_numpy(gnomad_data)

# Store the data as a dictionary    
gnomad_data_dict = dict()

for row in gnomad_data_np:
    gene = row[0]
    gnomad_data_dict[gene] = [
                            row[1], 
                            row[2], 
                            row[3], 
                            ]

# Format the dictionaries into data tables

print("\t".join(['gene_symbol', 
                    'observed_syn', 
                    'expected_syn', 
                    'syn_OE', 
                    'observed_nonsyn', 
                    'expected_nonsyn', 
                    'nonsyn_OE', 
                    'observed_DoF', 
                    'expected_DoF', 
                    'DoF_OE',
                    'LOEUF_score',
                    'gnom_observed_LoF', 
                    'gnom_expected_LoF', 
                    'gnom_LoF_OE', 
                    'gnomad_LOEUF_bin' #, 
#                    'LOEUF_score'
                    ]
                    )
                    )

for gene_name in all_genes.keys():

    if all_genes[gene_name] != 2:  # either there were no syn mutants in this gene, or there was no canonical transcript for this gene
        continue

    if not gene_name in canonical_transcript_by_gene.keys():
        continue
    
    if not gene_name in gnomad_data_dict.keys():
        continue
    
    exp_syn = expected_syn_by_gene[gene_name][0][0]
    exp_nonsyn = expected_nonsyn_by_gene[gene_name][0][0]
    exp_dof = expected_misLoF_by_gene[gene_name][0][0]
    
    obs_syn = obs_syn_by_gene[gene_name]
    obs_nonsyn = obs_nonsyn_by_gene[gene_name]
    obs_dof = obs_misLoF_by_gene[gene_name]
    
    gnom_exp_lof = gnomad_data_dict[gene_name][2]
    gnom_obs_lof = gnomad_data_dict[gene_name][1]
    
    gnomad_louef_bin = gnomad_data_dict[gene_name][0]
    
        
    # Calculate the OE Ratios for all pairs of data
    if exp_syn != 0:
        syn_oe = obs_syn/exp_syn
    else:
        syn_oe = np.nan
    
    if exp_nonsyn != 0:
        nonsyn_oe = obs_nonsyn/exp_nonsyn
    else:
        nonsyn_oe = np.nan
        
    if exp_dof != 0:
        dof_oe = obs_dof/exp_dof
        
        # Calculate the raw LOUEF score (not bin)
        louef_list = []
        x = 0
        while x <= 2:
            x += 0.02
            
            if len(louef_list) != 0:
                c_sum_prev = louef_list[-1]
            else:
                c_sum_prev = 0
                
            curr_poisson = poisson.pmf(obs_dof, exp_dof*x) + c_sum_prev
            louef_list.append(curr_poisson)
            
        louef_array = np.array(louef_list, dtype='float')
        normaliser = louef_array[-1]
        louef_array = np.divide(louef_array, normaliser)
        
        loeuf_score = 2
        x = 0
        for score in louef_array:
            x += 0.02
            if score > 0.95:
                loeuf_score = x
                break
        
    else:
        dof_oe = np.nan
        louef_score = np.nan
        
    if gnom_exp_lof != 0:
        gnom_lof_oe = gnom_obs_lof/gnom_exp_lof
    else:
        gnom_lof_oe = np.nan
        

    # Print this to STDOUT, with the gene symbol
    print("\t".join([
        gene_name, 
        str(obs_syn), 
        str(exp_syn), 
        str(syn_oe), 
        str(obs_nonsyn), 
        str(exp_nonsyn), 
        str(nonsyn_oe), 
        str(obs_dof), 
        str(exp_dof), 
        str(dof_oe),
        str(loeuf_score),
        str(gnom_obs_lof), 
        str(gnom_exp_lof), 
        str(gnom_lof_oe), 
        str(gnomad_louef_bin)#, 
#        str(loeuf_score)
        ]))
    

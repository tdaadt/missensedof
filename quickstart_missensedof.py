from MissenseLoFTools import TrinucleotideMutationRates
from MissenseLoFTools import ExpectedMutations
from ObservedMutationsGnomADr211 import ObservedMutations
from CanonicalTranscripts_b37p13 import CanonicalTranscriptome        
import getopt, sys

"""
An example implementation of the classes within MissenseDoF

Prints this data to STDOUT

Usage: python quickstart_missense_dof.py --upper_ddG_thresh 1.0 --lower_ddG_thresh -0.5 --data_dir /path/to/data 
          --maestro_out_dir /path/to/maestro_results_dir --gnomad_vcf /path/to/gnomad_vcf_file  
          --downsample_to_1_in_every 5 > minus0.5toDDGto1.0_ds1in5.tsv

Command options:
    upper_ddG_thresh: threshold for largest ddG value, beyond which substituions disrupt function
    lower_ddG_thresh: threshold for smalles ddG value, below which substituions disrupt function
    repo_data_dir: path to the 'data' subdirectory in the missensedof repository, contains data files required by missensedof
    maestro_out_dir: path to a directory containing all MAESTRO prediction output files or all proteins (download from https://stabilitysort.org/download
    gnomad_vcf: the observed variation from GnomAD r2.1.1 (download from https://gnomad.broadinstitute.org/downloads#v2 )
    downsample_to_1_in_every: downsampling rate


"""


# REPO DATA FILES

TRINUC_FREQ_FILENAME = 'GnomAD_2020_Karczewski_etal_supplementary_dataset_10_mutation_rates_full_trinucl.tsv'
ID_MAPPINGS_FILENAME = 'Hsa_ensg_enst_uniprot_genesymb_b38p13_canonical.tsv'
CANONICAL_TRANSC_FILENAME = 'Human_b37_p13_canonical_transcripts_220308.fa'

# Global ddG threshold DEFAULTS

UPPER_DDG_THRESH = 0.5 
LOWER_DDG_THRESH = -0.5
DOWNSAMPLE_TO_1_IN_EVERY = 1 # default is no downsampling
DATA_DIR = './data/' 
GNOMAD_VCF_FILENAME = None
MAESTRO_OUT_DIR = None

# (0) Get named command-line arguments, if any

commandArgs = sys.argv[1:] # keep args, minus the first one, which is the script name
print(sys.argv)
options = 'ulrmgd:'
long_options = ['upper_ddG_thresh=', 'lower_ddG_thresh=', 'repo_data_dir=', 'maestro_out_dir=', 'gnomad_vcf=', 'downsample_to_1_in_every=']

try:
    # getopt to parse arguments
    args, vals = getopt.getopt(commandArgs, options, long_options)

    # work through each argument and assign the passed value
    for current_opt, current_val in args:
        if current_opt in ["-u", "--upper_ddG_thresh"]:
            UPPER_DDG_THRESH = float(current_val)
        elif current_opt in ["-l", "--lower_ddG_thresh"]:
            LOWER_DDG_THRESH = float(current_val)
        elif current_opt in ["-d", "--downsample_to_1_in_every"]:
            DOWNSAMPLE_TO_1_IN_EVERY = int(current_val)
        elif current_opt in ["-r", "--repo_data_dir"]:
            DATA_DIR = current_val
        elif current_opt in ["-m", "--maestro_out_dir"]:
            MAESTRO_OUT_DIR = current_val
        elif current_opt in ["-g", "--gnomad_vcf"]:
            GNOMAD_VCF_FILENAME = current_val



except getopt.error as err:
    # output error, and return with an error code
    print (str(err))

print("upper_ddG_threshold: "+str(UPPER_DDG_THRESH))
print("lower_ddG_threshold: "+str(LOWER_DDG_THRESH))
print("downsample_to_1_in_every: "+str(DOWNSAMPLE_TO_1_IN_EVERY))
print("repo data directory: "+DATA_DIR)
print("MAESTRO out dir: "+MAESTRO_OUT_DIR)
print("GnomAD VCF file: "+GNOMAD_VCF_FILENAME)


# (1) Parse empirical trinucleotide frequencies from Karczewski et al

tmr = TrinucleotideMutationRates(trinuc_freq_file = DATA_DIR+"/"+TRINUC_FREQ_FILENAME)

# (2) Count the total synonymous variants for all genes (in the canonical transcript of each gene)

gnomad_mutations = ObservedMutations(
        observed_variation_file = GNOMAD_VCF_FILENAME, 
        id_mapping_file = DATA_DIR + "/" +ID_MAPPINGS_FILENAME,
        verbose = False,
        maestro_out_dir = MAESTRO_OUT_DIR
        )

gnomad_mutations.parse_observed_input(
            upper_ddG_threshold=UPPER_DDG_THRESH, 
            lower_ddG_threshold=LOWER_DDG_THRESH, 
            downsample_to_1_in_every=DOWNSAMPLE_TO_1_IN_EVERY
            ) ### this is were the heavy lifting with the GnomAD input VCF takes place - can run for ages!

total_synonymous_variants = 0
total_missense_variants = 0
total_missense_lof_variants = 0

obs_syn_by_gene = dict()
obs_nonsyn_by_gene = dict()
obs_misLoF_by_gene = dict()

canonical_transcript_by_gene = dict()

for enst in gnomad_mutations.transcript_gene.keys(): # loop through the canonical transcript for each gene and count the observed GnomAD r2.1.1 variants (that are from the GnomAD VCF file)

    # skip genes without variants - a get out of jail when using testing with a sub-sampled test input file
    if gnomad_mutations.get_observed_synonymous_mutation_count_for_transcript(enst) is None: 
        continue

    this_gene_name = gnomad_mutations.transcript_gene[enst]
    obs_syn_by_gene[this_gene_name] = gnomad_mutations.get_observed_synonymous_mutation_count_for_transcript(enst)
    obs_nonsyn_by_gene[this_gene_name] = gnomad_mutations.get_observed_missense_mutation_count_for_transcript(enst)
    obs_misLoF_by_gene[this_gene_name] = gnomad_mutations.get_observed_missense_LoF_count_for_transcript(enst)

    total_synonymous_variants += obs_syn_by_gene[this_gene_name]
    total_missense_variants += obs_nonsyn_by_gene[this_gene_name]
    total_missense_lof_variants += obs_misLoF_by_gene[this_gene_name]

    canonical_transcript_by_gene[this_gene_name] = enst
    
    
print("Total synonymous variants    : " + str(total_synonymous_variants))
print("Total missense variants      : " + str(total_missense_variants))
print("Total missense LoF variants  : " + str(total_missense_lof_variants))

# (3) Calculate sum of all the expected trinucleotide mutation frequencies that have a synonymous outcome

em = ExpectedMutations(upper_ddG_threshold=UPPER_DDG_THRESH, lower_ddG_threshold=LOWER_DDG_THRESH, maestro_out_dir=MAESTRO_OUT_DIR)

#-- Iterate through ALL CANONICAL TRANSCRIPT sequences (the input is _only_ canonical transcripts), counting possible synonymous mutations

ct = CanonicalTranscriptome()
ct.read_transcripts_from_fasta_file(input_fasta = DATA_DIR + '/' + CANONICAL_TRANSC_FILENAME) 

syn_relative_by_gene = dict()
nonsyn_relative_by_gene = dict()
misLoF_relative_by_gene = dict()
total_relative_syn = 0
total_possible_syn_muts_by_gene = dict()
total_possible_nonsyn_muts_by_gene = dict()
total_possible_misLoF_muts_by_gene = dict()

for gene_name in ct.transcripts_by_gene_name.keys(): # loop through the canonical transcript for each gene and count the observed GnomAD r2.1.1 variants (that are from the GnomAD VCF file)

    transcript = ct.transcripts_by_gene_name[gene_name]
    
    # skip genes without GnomAD variants - a get out of jail when using testing with a sub-sampled GnomAD test input file
    if gnomad_mutations.get_observed_synonymous_mutation_count_for_transcript(transcript.enst) is None:
        continue

    # Check that the canonical transcript parsed here with the CanonicalTranscriptome class actually matches with the canonical transcript parsed from the GnomAD VCF file

    if transcript.gene_name in canonical_transcript_by_gene.keys():

        fasta_seqs_ct = transcript.enst
        gnomad_vcf_ct = canonical_transcript_by_gene[transcript.gene_name]

    else:
        continue # for now, skip genes/transcripts that arent included in the GnomAD snippet file
            
    
    # Important - use the ExpectedMutation class (from MissenseLoFTools) to process this transcript to derive the trinucleotide site counts
    em.process_transcript(transcript.gene_name, transcript.uniprot_id, transcript.enst, transcript.seq)
    
    #-- COUNT the possible variants of each type present in this transcript

    #----- Count possible SYNONYMOUS variant sites per transcript

    gene_syn_trinucl_dict = em.get_possible_syn_trinucl_mutations_by_gene(gene_sym = transcript.gene_name)

    for ref_trinucl in gene_syn_trinucl_dict.keys():

        if ref_trinucl not in gene_syn_trinucl_dict.keys(): # this ref trinucl doesnt exist in this gene, strange but possible
            continue
            
        for var_trinucl in gene_syn_trinucl_dict[ref_trinucl].keys():

            if var_trinucl not in gene_syn_trinucl_dict[ref_trinucl].keys(): # the ref trinucl is observed, but there are no mut trinucls attached.  Something went wrong...
                raise Exception("In gene [" + gene_name + "], there are no mutants attached to the ref trinucl [" + ref_trinucl + "]")

            this_possible_syn_trinucl_count = gene_syn_trinucl_dict[ref_trinucl][var_trinucl]

            if gene_name in total_possible_syn_muts_by_gene.keys():
                total_possible_syn_muts_by_gene[gene_name] += this_possible_syn_trinucl_count
            else:
                total_possible_syn_muts_by_gene[gene_name] = this_possible_syn_trinucl_count
                
            this_tn_mut_rate = tmr.get(ref_trinucl, var_trinucl)
            this_relative_syn_per_gene = this_possible_syn_trinucl_count * float(this_tn_mut_rate)

            if gene_name in syn_relative_by_gene.keys():
                syn_relative_by_gene[gene_name] += this_relative_syn_per_gene
            else:
                syn_relative_by_gene[gene_name] = this_relative_syn_per_gene

            total_relative_syn += this_relative_syn_per_gene

    #----- Count possible NON-SYNONYMOUS variant sites per transcript
    #-------> calculate the _relative_ non-synonymous proportions of sites per gene

    gene_nonsyn_trinucl_dict = em.get_possible_nonsyn_trinucl_mutations_by_gene(gene_sym = transcript.gene_name)

    for ref_trinucl in gene_nonsyn_trinucl_dict.keys():

        if ref_trinucl not in gene_nonsyn_trinucl_dict.keys(): # this ref trinucl doesnt exist in this gene, strange but possible
            continue
            
        for var_trinucl in gene_nonsyn_trinucl_dict[ref_trinucl].keys():

            if var_trinucl not in gene_nonsyn_trinucl_dict[ref_trinucl].keys(): # the ref trinucl is observed, but there are no mut trinucls attached.  Something went wrong...
                raise Exception("In gene [" + gene_name + "], there are no mutants attached to the ref trinucl [" + ref_trinucl + "]")

            this_possible_nonsyn_trinucl_count = gene_nonsyn_trinucl_dict[ref_trinucl][var_trinucl]

            if gene_name in total_possible_nonsyn_muts_by_gene.keys():
                total_possible_nonsyn_muts_by_gene[gene_name] += this_possible_nonsyn_trinucl_count
            else:
                total_possible_nonsyn_muts_by_gene[gene_name] = this_possible_nonsyn_trinucl_count
                
            this_tn_mut_rate = tmr.get(ref_trinucl, var_trinucl)
            this_relative_nonsyn_per_gene = this_possible_nonsyn_trinucl_count * float(this_tn_mut_rate)

            if gene_name in nonsyn_relative_by_gene.keys():
                nonsyn_relative_by_gene[gene_name] += this_relative_nonsyn_per_gene
            else:
                nonsyn_relative_by_gene[gene_name] = this_relative_nonsyn_per_gene


    #----- Count possible MISSENSE LOF variant sites per transcript
    #-------> calculate the _relative_ missense LoF proportions of sites per gene

    gene_misLoF_trinucl_dict = em.get_possible_missense_lof_trinucl_mutations_by_gene(gene_sym = transcript.gene_name)

    for ref_trinucl in gene_misLoF_trinucl_dict.keys():

        if ref_trinucl not in gene_misLoF_trinucl_dict.keys(): # this ref trinucl doesnt exist in this gene, strange but possible
            continue
            
        for var_trinucl in gene_misLoF_trinucl_dict[ref_trinucl].keys():

            if var_trinucl not in gene_misLoF_trinucl_dict[ref_trinucl].keys(): # the ref trinucl is observed, but there are no mut trinucls attached.  Something went wrong...
                raise Exception("In gene [" + gene_name + "], there are no mutants attached to the ref trinucl [" + ref_trinucl + "]")

            this_possible_misLoF_trinucl_count = gene_misLoF_trinucl_dict[ref_trinucl][var_trinucl]

            if gene_name in total_possible_misLoF_muts_by_gene.keys():
                total_possible_misLoF_muts_by_gene[gene_name] += this_possible_misLoF_trinucl_count
            else:
                total_possible_misLoF_muts_by_gene[gene_name] = this_possible_misLoF_trinucl_count
                
            this_tn_mut_rate = tmr.get(ref_trinucl, var_trinucl)
            this_relative_misLoF_per_gene = this_possible_misLoF_trinucl_count * float(this_tn_mut_rate)

            if gene_name in misLoF_relative_by_gene.keys():
                misLoF_relative_by_gene[gene_name] += this_relative_misLoF_per_gene
            else:
                misLoF_relative_by_gene[gene_name] = this_relative_misLoF_per_gene

                
print("Total relative synonymous number: " + str(total_relative_syn))



# (4) Total observed synonymous count versus the total relative synonymous number - what is the numerical relationship?

multiplier = total_synonymous_variants / total_relative_syn

# (5) Apply multiplier to each 'per gene relative syn'

expected_syn_by_gene = dict()
expected_nonsyn_by_gene = dict()
expected_misLoF_by_gene = dict()

all_genes = dict()

# -- Genes set1
genes_set1 = list(obs_syn_by_gene.keys())
for gene in genes_set1:
    all_genes[gene] = 1

# -- Genes set2
genes_set2 = list(total_possible_syn_muts_by_gene.keys())
for gene in genes_set2:
    if gene in all_genes.keys():
        all_genes[gene] = 2
    else:
        all_genes[gene] = 1

# -- Iterate through all genes observed so-far and print some data

print("\t".join(('Gene', 'CanonicalTrans', 'Filter_Result', 'ObsSyn', 'ExpSyn', 'ObsNonsyn', 'ExpNonsyn', 'ObsMisLoF','ExpMisLoF')))

for gene_name in all_genes.keys():

    if all_genes[gene_name] != 2:  # either there were no syn mutants in this gene, or there was no canonical transcript for this gene
        continue

    if not gene_name in canonical_transcript_by_gene.keys():
        continue
    
    expected_syn_by_gene[gene_name] = syn_relative_by_gene[gene_name] * multiplier
    expected_nonsyn_by_gene[gene_name] = nonsyn_relative_by_gene[gene_name] * multiplier
    expected_misLoF_by_gene[gene_name] = misLoF_relative_by_gene[gene_name] * multiplier

    # --- hard filters applied at the last minute

    filter_reasons = list()

    if abs( obs_syn_by_gene[gene_name]/expected_syn_by_gene[gene_name] - 1) > 0.5:  # filter genes whether the obs/exp syn ratio is different by more than 50% --- something funny going on with these genes
        filter_reasons.append('fail_syn_oe_ratio')

    if expected_misLoF_by_gene[gene_name] < expected_nonsyn_by_gene[gene_name] * 0.1: # filter genes out that have have a count of expected missense LoF variants that is less the 10% of the possible missense variants - so low that there is probably something odd happening
        filter_reasons.append('fail_low_exp_mislof')

    if obs_nonsyn_by_gene[gene_name] < expected_nonsyn_by_gene[gene_name] * 0.3: # remove genes with a very low number of observed missense variants compared to the expected missense variants - this should be at least 30% (because at least this perc of missense variants are benign), so something odd happening if not greater than this
        filter_reasons.append('fail_low_obs_missense')

    if expected_syn_by_gene[gene_name] > 400: # remove long genes/proteins - because, at very least, MAESTRO doesn't run on proteins over 500aa without spliting them (and we haven't put them back together yet)
        filter_reasons.append('fail_long_prot')

    if len(filter_reasons) < 1:
        filter_reasons.append('pass')

    # --- end hard filters
    
    print("\t".join((gene_name,
                     canonical_transcript_by_gene[gene_name],
                     ','.join(filter_reasons),
                     str(obs_syn_by_gene[gene_name]),
                     str(expected_syn_by_gene[gene_name]),
                     str(obs_nonsyn_by_gene[gene_name]),
                     str(expected_nonsyn_by_gene[gene_name]),
                     str(obs_misLoF_by_gene[gene_name]),
                     str(expected_misLoF_by_gene[gene_name])
                     )))
